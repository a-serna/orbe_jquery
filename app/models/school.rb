class School < ActiveRecord::Base
  has_many :groups
  belongs_to :user

  # named scopes
  scope :by_user, -> (user_id) { where("user_id = ?", "#{user_id}")}
  scope :by_id, -> (id) { where("id = ?", "#{id}") }
end
