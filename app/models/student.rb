class Student < ActiveRecord::Base
  belongs_to :user
  has_one :group, through: :schools

  # validaciones
  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :age, presence: true
  validates :disability, presence: true

  # named scopes
  scope :by_user, -> (user_id) { where("user_id = ?", "#{user_id}")}
  scope :by_id, -> (id) { where("id = ?", "#{id}") }
end
