class SchoolsController < ApplicationController
  # metodo con scope que busque por usuario.
  # scope :school, where(@school.each(current_user))
  # def index
  #   @schools = School.each do
  #     current_user
  #   end
  #   flash[:error] = 'No has registrado ninguna institución.' if @schools.empty?
  # end
  #
  def to_s
    "institution_name:#{self.institution_name}"
  end

  def index
    @user = User.find(params[:user_id])
    @schools = @user.school
    flash[:error] = 'No vehicles registered.' if @schools.empty?
  end

  def new
    @school = School.new
  end

  def create
    @school = School.new(school_params)
    if @school.save
      flash[:success] = 'Institución registrada éxitosamente.'
      redirect_to user_main_path(current_user)
    else
      flash[:error] = @school.errors.full_messages.join(',')
      render 'new'
    end
  end

  def edit
    @school = School.find(params[:id])
  end

  def update
    @school = School.find(params[:id])
    if @school.update_attributes(school_params)
      flash[:success] = 'Institución actualizada.'
      redirect_to user_main_path # organizar ruta
    else
      flash[:error] = @school.errors.full_messages.join(',')
      render 'edit'
    end
  end

  def destroy
    @school = School.find(params[:id])
    if @school.destroy
      flash[:success] = 'Información de institución eliminada.'
      redirect_to schools_path # organizar ruta
    else
      flash[:error] = @school.errors.full_messages.join(',')
      render 'index'
    end
  end

  def school_params
    params.require(:school).permit(:id, :institution_name)
  end
  private :school_params

end
