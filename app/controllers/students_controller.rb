class StudentsController < ApplicationController
  def index
    @students = current_user.students
    flash[:error] = 'No ha registrado ningun estudiantes.' if @students.empty?
  end

  def new
    @user = current_user
    @student = Student.new
  end

  def create
    @student = Student.new(student_params)
    @student.user_id = current_user.id
    if @student.save
           StudentMailer.new_student_email(current_user).deliver_now
      flash[:success] = 'Ha registrado un estudiante satisfactoriamente.'
      redirect_to user_students_path(current_user) # organizar esta ruta
    else
      flash[:error] = @student.errors.full_messages.join(',')
      render 'new'
    end
  end

  def edit
    @student = Student.by_id(params[:id]).by_user(current_user.id).first
  end

  def update
      @Student = Student.by_id(params[:id]).by_user(current_user.id).first
    if @Student.update_attributes(student_params)
      flash[:success] = 'Información del estudiante actualizada.'
      redirect_to user_students_path(current_user) # organizar esta ruta
    else
      flash[:error] = @student.errors.full_messages.join(',')
      render 'edit'
    end
  end

  def destroy
    @student = Student.by_id(params[:id]).by_user(current_user.id).first
    if @student.destroy
      flash[:success] = 'Información del estudiante eliminada.'
      redirect_to user_students_path(current_user) # organizar esta ruta
    else
      flash[:error] = @student.errors.full_messages.join(',')
      render 'index'
    end
  end

  def student_params
    params.require(:student).permit(:user_id, :first_name, :last_name, :age, :disability, :id)
  end
  private :student_params
end
