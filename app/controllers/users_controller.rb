class UsersController < ApplicationController
  # def show
  #   @user = current_user
  #   if @user.save
  #     flash[:success] = 'Usuario registrado éxitosamente.'
  #     redirect_to user_show_path(current_user)
  #   else
  #     flash[:error] = @user.errors.full_messages.join(',')
  #     redirect_to new_user_session_path
  #   end
  # end
  def show
   @user = current_user
   if @user && @user.first_login
     current_user.update_attributes(first_login: false)
     redirect_to user_welcome_path(current_user)
   elsif @user && !@user.first_login
     redirect_to user_main_path(current_user)
   else
     redirect_to new_user_session_path
   end
 end


 # organizar para cuando un usuario no este logueado se muestre show
 # hacer una migracion para status booleano

 # def main
 #  @user = current_user
 #  #  if @user && @user.sign_in
 #  #    current_user.update_attributes(sign_in: true)
 #  #    redirect_to user_main_path(current_user)
 #  #  else
 #  #    redirect_to root_path(@user)
 #  #  end
 #  end

end
