class GroupsController < ApplicationController
  def index
    @school = School.find(params[:school_id])
    @group_presenter = GroupPresenter.new(@school)
    @group_presenter.build_group_rows

    @group_presenter.groups
    flash[:error] = 'No hay grupos registrados.' unless @group_presenter.groups? # verificar si es necesario este metodo
  end

  def new
    @school = School.find(params[:school_id])
    @group = Group.new
  end

  def create
    @school = School.find(params[:school_id])
    begin
      Group.transaction do
        @groups_to_create = params[:slots].to_i

        flash[:success] = 'EL grupo fue creado.'
        redirect_to school_groups_path(@school) # verificar la ruta adecuada
      end
    rescue
      flash[:error] = "Error al crear un grupo."
      render 'new'
    end
  end

  def edit
    @school = School.find(params[:school_id])
    @group = Group.by_id(params[:id]).by_school(@school.id).first
  end

  def update
    @school = School.find(params[:school_id])
    @group = Group.by_id(params[:id]).by_school(@school.id).first
    if @group.update_attributes(group_params)
      flash[:success] = 'El grupo ha sido actualizado.'
      redirect_to school_groups_path(@school) # verificar esta ruta
    else
      flash[:error] = @slot.errors.full_messages.join(',')
      render 'edit'
    end
  end

  def destroy
    @school = School.find(params[:school_id])
    @group = Group.by_id(params[:id]).by_school(@school.id).first
    if @group.destroy
      flash[:success] = 'Información del grupo borrada.'
      redirect_to school_groups_path(@achool) # verificar esta ruta
    else
      flash[:error] = @group.errors.full_messages.join(',')
      render 'index'
    end
  end

  def group_params
    params.require(:group).permit(:school_id, :id)
  end
  private :group_params
end
