class ApplicationController < ActionController::Base
  # private
  #
  # # Overwriting the sign_out redirect path method
  # def after_sign_out_path_for(resource_or_scope)
  #   root_path
  # end

  # def after_sign_in_path_for(current_user)
  #   user_main_path
  # end
  # after_filter :store_action
  #
  # def store_action
  #   return unless request.get?
  #   if (request.path != "/users/sign_in" &&
  #       request.path != "/users/sign_up" &&
  #       request.path != "/users/password/new" &&
  #       request.path != "/users/password/edit" &&
  #       request.path != "/users/confirmation" &&
  #       request.path != "/users/sign_out" &&
  #       !request.xhr?) # don't store ajax calls
  #     store_location_for(:user, request.root_path)
  #   end
  # end
  #
  # def after_sign_out_path_for(current_user)
  #   new_user_session_path
  # end
  #
  # def after_sign_in_path_for(current_user)
  #   user_main_path
  # end


  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  # before_filter :authenticate_user!
  # before_action :configure_permitted_parameters, if: :devise_controller?

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:first_name,
                                                       :last_name,
                                                       :email,
                                                       :password]);
  end
  protected :configure_permitted_parameters

end
