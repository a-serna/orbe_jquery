class AddSignInToUser < ActiveRecord::Migration
  def change
    add_column :users, :sign_in, :boolean
  end
end
